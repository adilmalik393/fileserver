import os


def create_test_file(path, title):
    try:
        os.stat(path)
    except:
        os.mkdir(path)
    file = open(path + title, 'w')
    file.write('test123\n')
    file.close()
    file = open(path + title, 'rb')
    return {'file': file, 'title':title}
