from rest_framework.authentication import BasicAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import parser_classes
from rest_framework import status
from rest_framework.parsers import FileUploadParser
from fileserver.settings import MAX_UPLOAD_SIZE
from files.models import Document
from files.api.serializers import DocumentSerializer


class DocumentView(APIView):
    parser_classes(FileUploadParser,)
    authentication_classes = (BasicAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def post(self, request):
        file = request.data.get('file', '')
        title = request.data.get('title', 'file')
        file_type = type(file)
        if file_type == unicode or file_type == str or file._size > MAX_UPLOAD_SIZE:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        try:
            document = Document(owner=request.user, data=file, title=title)
            document.save()
        except Exception as e:
            return Response(status=status.HTTP_500_INTERNAL_SERVER_ERROR)
        else:
            return Response(DocumentSerializer(document).data,
                            status=status.HTTP_201_CREATED)

    def get(self, request):
        files = Document.objects.filter(owner=request.user)
        serializer = DocumentSerializer(files, many=True)
        return Response(serializer.data)
