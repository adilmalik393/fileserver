from django.conf.urls import url
from files.api.views import DocumentView


urlpatterns = [
    url(r'^documents/$', DocumentView.as_view(), name='files-api-documents'),
]
