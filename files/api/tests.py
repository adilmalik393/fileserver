from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.test import APIClient
from django.test import TestCase
from django.contrib.auth.models import User
from fileserver.settings import TEST_FILE_DIRECTORY
from files.models import Document
from files.helpers import create_test_file

API_URL = reverse('files-api-documents')


class DocumentPostTests(TestCase):
    def setUp(self):
        user = User.objects.create_user('foo', 'foo@bar.com', 'foobar')
        user.save()

    def tearDown(self):
        Document.objects.all().delete()
        User.objects.get(username='foo').delete()

    def test_upload_valid_file_with_credentials(self):
        data = create_test_file(TEST_FILE_DIRECTORY , 'file')
        client = APIClient()
        client.login(username='foo', password='foobar')
        response = client.post(API_URL, data, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def test_upload_invalid_file_with_credentials(self):
        data = {'file':'file'}
        client = APIClient()
        client.login(username='foo', password='foobar')
        response = client.post(API_URL, data, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

    def test_upload_valid_file_without_credentials(self):
        data = create_test_file(TEST_FILE_DIRECTORY , 'file')
        client = APIClient()
        response = client.post(API_URL, data, format='multipart')
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)


class DocumentGetTests(TestCase):
    docs = ['file1', 'file2']
    def setUp(self):
        user = User.objects.create_user('foo', 'foo@bar.com', 'foobar')
        user.save()
        client = APIClient()
        client.login(username='foo', password='foobar')
        responses = [client.post(API_URL,
                                 create_test_file(TEST_FILE_DIRECTORY, doc),
                                 format='multipart') for doc in DocumentGetTests.docs]
        for response in responses:
            self.assertEqual(response.status_code, status.HTTP_201_CREATED)

    def tearDown(self):
        Document.objects.all().delete()
        User.objects.get(username='foo').delete()

    def test_get_files_test_with_credentials(self):
        client = APIClient()
        client.login(username='foo', password='foobar')
        response = client.get(API_URL)
        responses_files = [file.get('title') for file in response.data]
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(responses_files, DocumentGetTests.docs)

    def test_get_files_test_without_credentials(self):
        client = APIClient()
        response = client.get(API_URL)
        self.assertEqual(response.status_code, status.HTTP_401_UNAUTHORIZED)





