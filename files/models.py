from __future__ import unicode_literals

from django.db import models
from django.contrib.auth.models import User
# Create your models here.


class Document(models.Model):
    owner = models.ForeignKey(User)
    title = models.CharField(max_length=30, blank=True)
    data = models.FileField()
    created = models.DateTimeField(auto_now_add=True)
