## Synopsis - (In Progress - build not available)

This repo is a file server providing endpoints for uploading and viewing documents from Amazon S3.

## Motivation

This repo is created for a job interview at Next Generation Karachi, Pakistan.

## Installation
Before using this project, Before be sure to have pip and virtualenv installed. To Install these requirements, please visit http://toranbillups.com/blog/archive/2011/12/22/How-to-install-pip-virtualenv-and-django-on-ubuntu/

You can use and contribute to this project with following steps.

    1.  pip vitualenv env
    2.  source env/bin/activate
    3.  git clone https://gitlab.com/adilmalik393/fileserver.git
    4.  cd fileserver
    5.  pip install -r requirements.txt

After installing dependencies, You need to export some repo related Environment Variables

    6.  export DJANGO_SECRET_ID='your-django-secret-key'                  (i)**
    7.  export MAX_UPLOAD_SIZE=5242880                                    # In Bytes
    8.  export TEST_FILE_DIRECTORY='/path/to/test/files/'                 # For Testing purposes

After this, You will also need to define AWS related Environment Variables

    9.  export AWS_ACCESS_KEY_ID='your-aws-access-key-id'
    10. export AWS_SECRET_ACCESS_KEY='your-aws-secret-access-key'
    11. export AWS_STORAGE_BUCKET_NAME='your-aws-bucket-name'

Now run below commands to complete the setup.

    12. python manage.py makemigrations
    13. python manage.py migrate
    14. python manage.py runserver

(i)** Generate new django secret key here http://www.miniwebtool.com/django-secret-key-generator/

## Usage Example

You need to have curl installed. You can also use POSTMAN. ***Please note that this Api require authentication.
So before going to test api, perform these commands to create dajngo user as user creation is still not supported usng api.

    1. cd fileserver
    2. python manage.py shell
    3. from django.contrib.auth.models import User
    4. user  = User.objects.create_user('foo', 'foo@bar.com', 'foobar')
    5. user.save()
    6. exit()

Now you have succesfully created user, So you can use curl to test files api. Also keep in mind that user can't
access anyone else's files.

    POST Request Structure ::

    curl -u foo:foobar -F "title=title" -F "file=@/path/to/file" 127.0.0.1:8000/files/api/documents/

    Corresponding Response Structure ::

    {"title":"title of your file","data":"amazon-s3-based-url","created":"2017-03-22T03:35:21.700561Z"}

    GET Request Stucture ::

    curl -u foo:foobar 127.0.0.1:8000/files/api/documents/

    Corresponding Response Structure ::

    [
        {"title":"file1","data":"aws-based-url1","created":"2017-03-22T02:13:41.949126Z"},
        {"title":"file2","data":"aws-based-url2","created":"2017-03-22T02:14:15.295324Z"},
        {"title":"file3","data":"aws-based-url3","created":"2017-03-22T02:39:30.280344Z"}
    ]

## Coming Soon

    1. Packaging via pip.

## Acknowledgements

    API :: http://www.django-rest-framework.org/

    S3  :: https://django-storages.readthedocs.io/en/latest/

    README :: https://gist.github.com/jxson/1784669

    Tests  :: https://docs.djangoproject.com/en/1.10/topics/testing/

## Tests

To run tests, Please go under /fileserver and run below commands.

    python manage.py test
